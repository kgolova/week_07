package ru.edu;

import junit.framework.TestCase;
import ru.edu.model.Symbol;

public class BinanceSymbolPriceServiceTest extends TestCase {

    public void testGetPrice() {

        SymbolPriceService service = new BinanceSymbolPriceService();

        Symbol price = service.getPrice("BTCUSDT");

        assertNotNull(price);

    }

    public void testGetPriceUnknow() {

        SymbolPriceService service = new BinanceSymbolPriceService();

        Symbol price = service.getPrice("SYMBOL");

        assertNull(price);

    }
}