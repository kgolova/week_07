package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.model.Symbol;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CashedSymbolServiceImpl implements SymbolPriceService {

    /**
     * Сервис кэшированных валют.
     */
    private final SymbolPriceService delegete;

    /**
     * Кэш для хранения курсов валют.
     */
    private Map<String, Symbol> cache =
            Collections.synchronizedMap(new HashMap<>());

    /**
     * Фабрика логов.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(CashedSymbolServiceImpl.class);

    /**
     * Время актуальности информации в кэше.
     */
    public static final int TIMEOUT = 10;

    /**
     * Конструктор.
     * @param symbolService
     */
    public CashedSymbolServiceImpl(final SymbolPriceService symbolService) {
        this.delegete = symbolService;
    }

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого
     * данные будут обновляться не чаще, чем раз в 10 секунд.
     *
     * @param symbolName
     * @return symbol
     */
    @Override
    public Symbol getPrice(final String symbolName) {

        synchronized (symbolName) {
            if (!cache.containsKey(symbolName)
                    || Instant.now().minus(TIMEOUT, ChronoUnit.SECONDS).isAfter(
                    cache.get(symbolName).getTimeStamp())) {

                Symbol price = delegete.getPrice(symbolName);

                cache.put(symbolName, price);

            }
        }
        return cache.get(symbolName);
    }

}
