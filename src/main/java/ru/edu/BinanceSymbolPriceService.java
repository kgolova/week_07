package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.Symbol;
import ru.edu.model.SymbolImpl;

import java.time.Instant;

public class BinanceSymbolPriceService implements SymbolPriceService {

    /**
     * Шаблон сервиса.
     */
    private RestTemplate restTemplate = new RestTemplate();

    /**
     * Фабрика логов.
     */
    private static final Logger LOGGER =
            LoggerFactory.getLogger(BinanceSymbolPriceService.class);

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого данные будут
     * обновляться не чаще, чем раз в 10 секунд.
     *
     * @param symbolName
     * @return symbol
     */
    @Override
    public Symbol getPrice(final String symbolName) {

        String url = "https://api.binance.com/api/v3/ticker/price?symbol="
                        + symbolName;

        try {
            ResponseEntity<SymbolImpl> response =
                    restTemplate.getForEntity(url, SymbolImpl.class);
            if (response.getStatusCode() != HttpStatus.OK) {
                LOGGER.error("Ошибка вызова сервиса. Код ответа {}",
                        response.getStatusCode());
                return null;
            }

            LOGGER.debug("Успешный вызов сервиса {}. Код ответа {}", symbolName,
                    response.getStatusCode());

            SymbolImpl symbol = response.getBody();
            symbol.setTimeStamp(Instant.now());

            return symbol;
        } catch (RestClientException ex) {
            ex.getStackTrace();
            return null;
        }


    }
}
