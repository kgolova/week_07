package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

public class SymbolImpl implements Symbol {

    /**
     * symbol.
     */
    private String symbol;

    /**
     * price.
     */
    private BigDecimal price;

    /**
     * timeStamp.
     */
    private Instant timeStamp;


    /**
     * Getter.
     * @return symbol
     */
    @Override
    public String getSymbol() {
        return symbol;
    }

    /**
     * Getter.
     * @return price
     */
    @Override
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Время получения данных.
     *
     * @return
     */
    @Override
    public Instant getTimeStamp() {
        return timeStamp;
    }

    /**
     * Setter.
     * @param symboltmp
     */
    public void setSymbol(final String symboltmp) {
        this.symbol = symboltmp;
    }

    /**
     * Setter.
     * @param pricetmp
     */
    public void setPrice(final BigDecimal pricetmp) {
        this.price = pricetmp;
    }

    /**
     * Setter.
     * @param timeStamptmp
     */
    public void setTimeStamp(final Instant timeStamptmp) {
        this.timeStamp = timeStamptmp;
    }

    /**
     * toString.
     * @return toString
     */
    @Override
    public String toString() {
        return symbol + " \t" + timeStamp + " \t" + price + " \t";
    }

}
