package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * Интерфейс информации о курсе обмена.
 */
public interface Symbol {

    /**
     * Валюта.
     * @return Symbol
     */
    String getSymbol();

    /**
     * Курс валюты.
     * @return price
     */
    BigDecimal getPrice();

    /**
     * Время получения данных.
     *
     * @return timeStamp
     */
    Instant getTimeStamp();
}
